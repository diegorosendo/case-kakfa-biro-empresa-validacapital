package br.com.itau.consumervalidaproducer.producer;

import br.com.itau.producercadastra.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaValidaProducer {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        producer.send("spec3-diego-rosendo-3", empresa);
    }

}
