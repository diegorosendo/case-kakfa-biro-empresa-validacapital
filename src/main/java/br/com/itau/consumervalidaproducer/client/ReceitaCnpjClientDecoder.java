package br.com.itau.consumervalidaproducer.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ReceitaCnpjClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidReceitaCnpjException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
