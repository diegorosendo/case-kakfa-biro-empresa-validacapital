package br.com.itau.consumervalidaproducer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ReceitaFederal", url = "https://www.receitaws.com.br", configuration = ReceitaCnpjClientConfiguration.class)
public interface ReceitaCnpjClient {

    @GetMapping("/v1/cnpj/{numCnpj}")
    ReceitaCnpjClientRequest getByCnpj(@PathVariable String numCnpj);

}
