package br.com.itau.consumervalidaproducer.client;

public class ReceitaCnpjClientFallBack implements ReceitaCnpjClient {
    @Override
    public ReceitaCnpjClientRequest getByCnpj(String numCnpj) {
        throw new ReceitaCnpjOfflineException();
    }
}
