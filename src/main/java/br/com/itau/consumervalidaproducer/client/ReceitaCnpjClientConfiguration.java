package br.com.itau.consumervalidaproducer.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ReceitaCnpjClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new ReceitaCnpjClientDecoder();
    }


    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ReceitaCnpjClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }


}
