package br.com.itau.consumervalidaproducer.consumer;

import br.com.itau.consumervalidaproducer.client.ReceitaCnpjClient;
import br.com.itau.consumervalidaproducer.client.ReceitaCnpjClientRequest;
import br.com.itau.consumervalidaproducer.producer.EmpresaValidaProducer;
import br.com.itau.producercadastra.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.Random;

@Component
public class EmpresaConsumer {


    private final Double ONE_MILLION_CAPITAL_SOCIAL = 1000000.00;

    @Autowired
    private EmpresaValidaProducer empresaValidaProducer;

    @Autowired
    private ReceitaCnpjClient receitaCnpjClient;


    @KafkaListener(topics = "spec3-diego-rosendo-2", groupId = "Diego-zoom-mastertech")
    public void receber(@Payload Empresa empresa) {

        //..::[PASSO 01]::.. Receber cadastros de Empresas da fila
        System.out.println("RECEBI UMA EMPRESA COM CNPJ: " + empresa.getCnpj() + " NOME: " + empresa.getNome());


        //..::[PASSO 02]::.. Chamar API para capturar capital social
        // https://www.receitaws.com.br/v1/cnpj/60701190000104
        Boolean capitalEmpresaAcimaDe1Mi;
        capitalEmpresaAcimaDe1Mi = capitalEmpresaAcimaDe1Mi(empresa.getCnpj());
        System.out.println("Empresa com CNPJ:" + empresa.getCnpj() + " tem capital acima de 1MI: "+ capitalEmpresaAcimaDe1Mi);


        //..::[PASSO 03]::.. Cadastrar na Fila de Empresas Validas (Capital acima de 1MI)
        if (capitalEmpresaAcimaDe1Mi)
        {
            empresaValidaProducer.enviarAoKafka(empresa);
        }

    }

    private Boolean capitalEmpresaAcimaDe1Mi(String cnpj){

        ReceitaCnpjClientRequest cnpjConsulta = receitaCnpjClient.getByCnpj(cnpj);

        System.out.println("..:: Receita Federal Consulta ::.. -->CNPJ: " + cnpjConsulta.getCnpj() + " --> Capital Social" + cnpjConsulta.getCapitalSocial() );

        if (cnpjConsulta.getCapitalSocial() >= this.ONE_MILLION_CAPITAL_SOCIAL){
            return true;
        }
        else{
            return false;
        }

    }

}
