package br.com.itau.consumervalidaproducer.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O micro serviço ReceitaCNPJ está offline.")
public class ReceitaCnpjOfflineException extends RuntimeException {
}
