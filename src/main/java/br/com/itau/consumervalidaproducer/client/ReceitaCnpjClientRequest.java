package br.com.itau.consumervalidaproducer.client;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ReceitaCnpjClientRequest {

    private String cnpj;

    @JsonProperty("capital_social")
    private Double capitalSocial;

    public ReceitaCnpjClientRequest() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Double getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(Double capitalSocial) {
        this.capitalSocial = capitalSocial;
    }
}
